# Constants
CC=g++
O=bin
S=src
H=inc
CFLAGS=-lm -g  -W -Wall -Werror -O2 -Wno-unused-variable -Wno-unused-parameter
LFLAGS=`pkg-config --libs opencv fann` -I/usr/include/opencv -I/usr/include -lncurses 

# Compilation

all: $(O) example example2 fannoc

$(O):
	mkdir $(O)

fannoc:$(O)/utils.o $(O)/image.o $(O)/print.o $(O)/main.o
	$(CC) -o $@ $^ $(CFLAGS) $(LFLAGS)

example:$(O)/example.o
	$(CC) -o $@ $^ $(CFLAGS) $(LFLAGS)

example2:$(O)/example2.o
	$(CC) -o $@ $^ $(CFLAGS) $(LFLAGS)

$(O)/main.o: $(S)/main.c
	$(CC) -o $@ -c $^ $(CFLAGS) $(LFLAGS)

$(O)/print.o:$(S)/print.c
	$(CC) -o $@ -c $^ $(CFLAGS) $(LFLAGS)

$(O)/example.o:$(S)/example.c
	$(CC) -o $@ -c $^ $(CFLAGS) $(LFLAGS)

$(O)/example2.o:$(S)/example2.c
	$(CC) -o $@ -c $^ $(CFLAGS) $(LFLAGS)

$(O)/image.o:$(S)/image.c
	$(CC) -o $@ -c $^  $(CFLAGS) $(LFLAGS)

$(O)/utils.o:$(S)/utils.c
	$(CC) -o $@ -c $^ $(CFLAGS) $(LFLAGS)

# Others

clean:
	rm -rf $(O)/*.o

mrproper: clean
	rm -rf $(S)/.*.swp
	rm -rf $(H)/.*.swp
	rm -rf .*.swp
	rm -rf example
	rm -rf example2
	rm -rf fannoc
	cd doc && doxygen Doxyfile && cd ..
