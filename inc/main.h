/*!
 * \file main.h
 * \author blaoi@tuta.io
 * \version 1.0
 * \date spring 2017
 * \brief Contains the methods declaration.
 */
#ifndef __MAIN_H_
#define __MAIN_H_
#include <argp.h>
#include <ncurses.h>
#include <cv.h>
#include <dirent.h>
#include <errno.h>
#include <highgui.h>
#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
int verifyArguments(int argc, char ** argv);
void print_available_devices(void);
void display_realtime(const int camera_number);
void take_picture(const int camera_number, const char* path);
void take_pictures(const int camera_number, const char* path,const int ms);
int msleep(unsigned long milisec);
void deep_learning_training(const char* path);
void deep_learning_executing(const int camera_number,const char* filename);
int get_total_file(const char*path);
int get_total_directory(const char* path);
#endif
