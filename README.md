# Webcam openCV

A GNU/Linux command-like program that helps the user to manipulate webcam
thanks to openCV and C language.

# Installation

## STEP 1 - Install libraries

1-The openCV library will allow the program to process images.
You need to set it up manually from its website.

[OpenCV website](http://opencv.org)

or

[OpenCV repository](https://github.com/opencv/opencv)

2-The FANN library allow the program to learn images and to recognize
shape in it thanks to neuronal networks.
You need to set it up manually from its website.

[Fann website](http://leenissen.dk/fann/wp/)

or

[Fann repository](https://github.com/libfann/fann)

## STEP 2 - Download and compilation

To download the source files there is multiple ways. The simplest is:
	
	git clone https://_Blaoi@gitlab.com/_Blaoi/openCV_Webcam.git

	cd openCV_Webcam
	
	make

# Usage

## Print devices

This option print all the devices the program can use for capturing pictures/frames.
Internally, the program print only the list of /dev/videoX available.

	./main -p

# FAQ

# More

## Contact

For any issues, questions or anything related you can contact me:

mail:blaoi@tuta.io

[Twitter](https://twitter.com/_Blaoi)
