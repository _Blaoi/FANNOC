#include "../inc/main.h"

int get_total_file(const char* path){
	DIR *d, *d2;
	struct dirent *dir, *dir2;
	int sum = 0;
	d = opendir(path);
	if(d){
		while((dir = readdir(d)) != NULL){
			if(dir->d_type == DT_DIR
				&& strcmp(dir->d_name,".") != 0
				&& strcmp(dir->d_name,"..") != 0){
				d2 = opendir(dir->d_name);
				while((dir2 = readdir(d2)) != NULL){
					if(dir2->d_type == DT_REG){
						++sum;
					}
				}
			}
		}
	}else{
		perror("Error path");
	}
	return sum;
}

int get_total_directory(const char* path){
	DIR *d;
	struct dirent *dir;
	int sum = 0;
	d = opendir(path);
	if(d){
		while((dir = readdir(d)) != NULL){
			if(dir->d_type == DT_DIR
				&& strcmp(dir->d_name,".") != 0
				&& strcmp(dir->d_name,"..") != 0){
				++sum;
			}
		}
	}else{
		perror("Error path");
	} 
	return sum;
}
