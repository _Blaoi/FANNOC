/*!
 * \file main.c
 * \author blaoi@tuta.io
 * \version 1.0
 * \date spring 2017
 * \brief Contains the arguments parser with argp.
 */
#include "../inc/main.h"

unsigned int camera_number;
char* SAVE_PATH;
float FRAME_PER_SECOND = 24;
const char *argp_program_version = "Fannoc 1.0";
const char *argp_program_bug_address = "<blaoi@tuta.io>";

struct arguments{
	char *args[1];		//ARG1 *args[2] for ARG1 & ARG2 and so on...
	char *c_outpath;	//Argument for -c
	char *C_outpath;	//Argument for -C
	const char *T_inpath;		//Argument for -T
	const char *E_infile;		//Argument for -E
};

/*
	OPTIONS. Field 1 in ARGP.
	Order of fields: {NAME,KEY,ARG,FLAGS,DOC}
*/
struct argp_option options[] = 
{
	{"display",'d',0,0,"Display the camera from the device /dev/videoN, if no number specified the /dev/video0 is used. The '-d0' is equivalent to '-d'.\n",0},
	{"print",'p',0,0,"Print all the available devices for capturing images.",0},
	{"capture",'c',"PATH",0,"Capture a single image in the PATH.",0},
	{"captures",'C',"PATH",0,"Capture multiple images in the PATH until interruption.",0},
	{"deep-learning-training",'T',"folder",0,"Create the training file.",0},
	{"deep-learning-executing",'E',"filename",0,"Use the file for actual image recognition.",0},
	{0,0,0,0,0,0}
};

static bool p_flag = false;
static bool d_flag = false;
static bool D_flag = false;
static bool T_flag = false;

/*
	PARSER. Field 2 in ARGP.
	Order of parameters: KEY, ARG, STATE.
*/
static error_t parse_opt(int key,char *arg,struct argp_state *state){
	struct arguments *arguments = (struct arguments *)state->input;
	switch(key){
		case 'd':
			d_flag = true;
			break;
		case 'c':
			arguments->c_outpath = arg;
			take_picture(camera_number,arguments->c_outpath);
			break;
		case 'C':
			arguments->C_outpath = arg;
			take_pictures(camera_number,arguments->C_outpath,25);
			break;
		case 'p':
			p_flag = true;
			break;
		case 'T':
			T_flag = true;
			arguments->T_inpath = arg;
			break;
		case 'E':
			arguments->E_infile = arg;
			deep_learning_executing(camera_number,arguments->E_infile);
			break;
		case ARGP_KEY_ARG:
			if(state->arg_num >= 1){
				argp_usage(state);
			}
			arguments->args[state->arg_num] = arg;
			break;
		case ARGP_KEY_NO_ARGS:
			if(p_flag){
				print_available_devices();
			}else if(T_flag){
				deep_learning_training(arguments->T_inpath);
			}else{
				printf("No args\n");
			}
			break;
		case ARGP_KEY_END:
			if(state->arg_num < 1 && !p_flag && !T_flag){
				argp_usage(state);
			}
			if(d_flag){
				display_realtime(camera_number);
			}
			break;
		default:
			return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

/*
	ARGS_DOC. Field 3 in ARGP.
	A descritption of the non-option command-line arguments
	that we accept.
*/
static char args_doc[] = "CAMERA_NUMBER";

/*
	DOC. Field 4 in ARGP.
	Program documentation.
*/
static char doc[] = 
"FANNOC -- Stands for FANN and OpenCV, is a program to manipulate"
" camera from command-line and practice image recognition images.";

/*
	The ARGP structure itself.
*/

static struct argp argp = {options,parse_opt,args_doc,doc,0,0,0};

/*
	The main function.
*/
int main(int argc, char ** argv){
	struct arguments arguments;
	/* SET ARGUMENTS DEFAULTS*/
	camera_number = 0;
	argp_parse(&argp,argc,argv,0,0,&arguments);
	return 0;
}
