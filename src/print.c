/*!
 * \file print.c
 * \author blaoi@tuta.io
 * \version 1.0
 * \date spring 2017
 * \brief Contains all the methods that print/display something on the user's screen.
 */

#include "../inc/main.h"

void print_available_devices(void){
  DIR *dir;
  struct dirent *ent;
  int device_counter = 0;
  if ((dir = opendir ("/dev")) != NULL) {
    printf("Available devices:\n");
      while ((ent = readdir (dir)) != NULL) {
	if (strstr(ent->d_name, "video") != NULL) {
	  device_counter++;
	  printf ("%d: /dev/%s\n",device_counter,ent->d_name);
	}
      }
    closedir(dir);
  } else {
    perror ("Could not open directory /dev");
  }
}

void display_realtime(int camera_number){
  printf("Display realtime\n");
  printf("Press Escape to quit\n");
  CvCapture *capture;
  capture = cvCreateCameraCapture(camera_number);
  if(capture == NULL){
    fprintf(stderr,"no camera found");
    abort();
  }
  cvNamedWindow("Webcam RGB",CV_WINDOW_AUTOSIZE);
  IplImage *image = NULL;
  char key;
  while(1){
    image = cvQueryFrame(capture);
    cvShowImage("Webcam RGB",image);    
    key = cvWaitKey(10);
    if(key == 27){ //ESC KEY
      break;
    }
  }
  cvReleaseCapture(&capture);
  cvDestroyWindow("Webcam RGB");
}
