#include <stdio.h>
#include <stdlib.h>
#include <opencv2/highgui.hpp>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv/cxcore.h>

int main(void){
  CvCapture *capture;
  capture = cvCreateCameraCapture(0);
  if(capture == NULL){
    fprintf(stderr,"no camera found");
    return 1;
  }
  cvNamedWindow("Webcam RGB",CV_WINDOW_AUTOSIZE);
 
  IplImage *image = NULL;
  
  char key;
  while(1){
    image = cvQueryFrame(capture);
    cvShowImage("Webcam RGB",image);
    
    
    key = cvWaitKey(10);
    if(key == 27){ //ESC KEY
      break;
    }
  }
  cvReleaseCapture(&capture);
  cvDestroyWindow("Webcam RGB");
  
  return 0;
}
