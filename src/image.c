/*!
 * \file image.c
 * \author blaoi@tuta.io
 * \version 1.0
 * \date spring 2017
 * \brief Contains all the methods related to image processing.
 */

#include "../inc/main.h"
#include <cstdlib>
#include <opencv2/core/mat.hpp>
#include <limits.h>
#include <iostream>
#include <fstream>
#include <string>
#include <fann.h>
#include <iomanip>

using namespace cv;
using namespace std;
static volatile int keepRunning = 1;

void intHandler(int dummy){
	keepRunning = 0;
}

void deep_learning_training(const char *path){
	char filename[256];
	int image_width, image_height;
	if(chdir(path) == -1){
		fprintf(stderr,"%s: %s\n",path,strerror(errno));
		exit(EXIT_FAILURE);	
	}
	if(system("clear") == -1){}
	cout << "Deep learning - Training mode\n";
	cout << "File to create: ";
	cin >> filename;
	cout << "In " << path << endl;
	int total_file = get_total_file(path);
	int total_directory = get_total_directory(path);
	cout << "\tFiles:" << total_file << endl;
	cout << "\tDirectories:" << total_directory << endl;
	cout << "Dimensions of the pictures" << endl;	
	cout << "Witdh: "; cin >> image_width;
	cout << "Height:"; cin >> image_height;
	cout << "Dimensions:" << image_width << "x" << image_height << endl;
	cout << "Creation of the data file..." << endl;
	ofstream data_file;
	data_file.open(filename); 
	cout << "Writing header..." << endl;
	int training_pairs, number_of_input, number_of_output;
	training_pairs = get_total_file(path);
	number_of_input = image_width * image_height;
	number_of_output = 1;
	std::string file_header;
	std::stringstream ss;
	ss << training_pairs << " " << number_of_input << " " << number_of_output;
	file_header = ss.str();
	cout << file_header << endl; data_file << file_header << endl;
	cout << "Writing datas...";
	DIR *d, *d2;
	struct dirent *dir,*dir2;
	int number_of_image = 0, directory_images = 0, directory_number = 0;
	d = opendir(path);
	if(d){
		while((dir = readdir(d)) != NULL){
			if(dir->d_type == DT_DIR 
				&& strcmp(dir->d_name,".") != 0 
				&& strcmp(dir->d_name,"..") != 0){
				char* directory_parent_name = dir->d_name;
				char buffer_path[PATH_MAX+1];
				char* directory_parent_fullpath = realpath(directory_parent_name,buffer_path);	
				d2 = opendir(dir->d_name); 
				if(d2){
					while((dir2 = readdir(d2)) != NULL){
						if(dir2->d_type == DT_REG
						&& strcmp(dir2->d_name,".") != 0
						&& strcmp(dir2->d_name,"..") != 0){
							char complete_path[PATH_MAX];
							sprintf(complete_path,"%s%s%s",directory_parent_fullpath,"/",dir2->d_name);
							++directory_images;
							++number_of_image;
							Mat src = imread(complete_path);	
							for(int i = 0;i < src.rows;++i){
								for(int j = 0;j < src.cols;++j){
									int b = src.at<cv::Vec3b>(i,j)[0];
									int g = src.at<cv::Vec3b>(i,j)[1];
									int r = src.at<cv::Vec3b>(i,j)[2];
									data_file << "0x";
									data_file << setfill('0') << setw(2) << hex << r;
									data_file << setfill('0') << setw(2) << hex << g;
									data_file << setfill('0') << setw(2) << hex << b;
									if(i == src.rows - 1 && j == src.cols - 1){
									}else{
										data_file << " " ;
									}
								}
							}
							cout << std::setprecision(2);
							cout << ((float)number_of_image/(float)total_file)*100 <<"% - ";
							cout << number_of_image << "/" << total_file << endl;	
							data_file << endl << directory_number << endl;
						}						
					}
					printf("%s : %d files.\n",dir->d_name,directory_images);
					directory_images = 0;
					++directory_number;
					closedir(d2);
				}
			}
		}
		closedir(d);
	}
	data_file.close();
	printf("Total images: %d\n",total_file);
	const unsigned int num_input = number_of_input;
	const unsigned int num_output = number_of_output;
	const unsigned int num_layers = 1;
	const unsigned int num_neurons_hidden = 338;
	const float desired_error = (const float) 0.01;
	const unsigned int max_epochs = 1000000;
	const unsigned int epoch_between_reports = 1;
	unsigned int layers[4] = {num_input,8,9,num_output};
	struct fann *ann = fann_create_standard_array(4,layers);
	fann_set_activation_function_hidden(ann,FANN_SIGMOID_SYMMETRIC);
	fann_set_activation_function_output(ann,FANN_SIGMOID_SYMMETRIC);
	fann_train_on_file(ann,filename,max_epochs,epoch_between_reports,desired_error);
	fann_save(ann,"data.net");
	fann_destroy(ann);
}

void deep_learning_executing(const int camera_number, const char* filename){
	if(access(filename, F_OK ) == -1){
		fprintf(stderr,"%s\n",strerror(errno));
		abort();
	}
	printf("Deep learning - Executing mode\n");
	CvCapture *capture;
	capture = cvCreateCameraCapture(camera_number);
	if(capture == NULL){
		fprintf(stderr,"No camera found\n");
		abort();
	}
	cvNamedWindow("Deep learning - executing",CV_WINDOW_AUTOSIZE);
	signal(SIGINT,intHandler);
	char key;
	IplImage *image = NULL;
	while(keepRunning){
		image = cvQueryFrame(capture);
		if(image != NULL){
			cvShowImage("Deep learning - executing",image);
			fann_type *calc_out;
			struct fann *ann = fann_create_from_file(filename);
			cv::Mat src = cvarrToMat(image,true);
			const unsigned int number_of_pixel = src.cols * src.rows;
			fann_type input[number_of_pixel];
			for(int i = 0;i < src.rows;++i){
				for(int j = 0;j < src.cols;++j){
					int b = src.at<cv::Vec3b>(i,j)[0];
                    int g = src.at<cv::Vec3b>(i,j)[1];
                    int r = src.at<cv::Vec3b>(i,j)[2];
					std::stringstream ss;
					ss << "0x";
                    ss << setfill('0') << setw(2) << hex << r;
                   	ss << setfill('0') << setw(2) << hex << g;
                    ss << setfill('0') << setw(2) << hex << b;
					unsigned int tmp_int;
					ss >> tmp_int;
					input[i * src.cols + j] = (float)tmp_int;
				}
			}
			calc_out = fann_run(ann,input);
			printf("Result: %lf\n",calc_out[0]);
			fann_destroy(ann);
		}
		key = cvWaitKey(10);
		if(key == 27){
			keepRunning = 0;			
		}
	}
	cvReleaseCapture(&capture);
}

void take_picture(const int camera_number, const char* path){
	if(chdir(path) == -1){
		fprintf(stderr,"Path not correct:%s\n",strerror(errno));
		abort();
	}else{
		//directory exists
		CvCapture *capture;
		capture = cvCreateCameraCapture(camera_number);
		if(capture == NULL){
			fprintf(stderr,"No camera found\n");
			abort();
		}
		IplImage *image = NULL;
		image = cvQueryFrame(capture);
		cvSaveImage("testCapture.jpg",image);
		cvReleaseCapture(&capture);
	}
} 

void take_pictures(const int camera_number, const char* path,const int ms){
	if(chdir(path) == -1){
		fprintf(stderr,"Path not correct %s\n",strerror(errno));
		abort();
	}else{
		CvCapture *capture;
		IplImage *image = NULL;
		int i = 0;
		char key;
		capture = cvCreateCameraCapture(camera_number);
		if(capture == NULL){
			fprintf(stderr,"No camera found\n");
			abort();
		}

		signal(SIGINT,intHandler);
		while(keepRunning){
			char filename[] = "image_%d.jpg";
			char number[1000];
			sprintf(number,filename,i);
			image = cvQueryFrame(capture);
			msleep(ms);
			do{
				i++;
				sprintf(number,filename,i);
			}while(access(number,F_OK) != -1);
			cvSaveImage(number,image);
			printf("%s\n",number);
			key = cvWaitKey(10);
			if(key == 27){ //ESC KEY 
				keepRunning = 0;
			}
		}
		cvReleaseCapture(&capture);
	}
}

int msleep(unsigned long milisec){
	struct timespec req={0,0};
	time_t sec=(int)(milisec/1000);
	milisec=milisec-(sec*1000);
	req.tv_sec=sec;
	req.tv_nsec=milisec*1000000L;
	while(nanosleep(&req,&req)==-1)
		continue;
	return 1;
}
